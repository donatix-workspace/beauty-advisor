## Greate Offline Mode

1. Get Source for `index.html` from `view-source:http://<domain-name.com>/questions`
1. In index.html change

~~~
<link href="/css/app.css" rel="stylesheet">
~~~

with

~~~
<link href="css/app.css" rel="stylesheet">
~~~

and

~~~
<script src="js/app.js"></script>
~~~

with

~~~
<script src="js/app.js"></script>
~~~

1. Copy `public/js/app.js` from laravel repo to `js/app.js`
1. Copy `public/js/products.json` from laravel repo to `js/products.json`
1. Copy files in `public/img` to `img`
1. Copy files in `public/fonts` to `fonts`
1. Copy `public/css/app.css` from laravel repo to `css/app.css`

1. In `js/app.js` find

~~~
image: function image() {
      return 'url(../img/skins/' + this.type + '_' + this.value + '.png)';
},
~~~

and replace it with 

~~~
image: function image() {
      return 'url(img/skins/' + this.type + '_' + this.value + '.png)';
},
~~~

1. In `js/app.js` find

~~~
 productImageUrl: function productImageUrl(productId) {
            var url = '../img/products/' + productId + '.png';
            return this.imagesMappings.hasOwnProperty(url) ? this.imagesMappings[url] : url;
}
~~~

and replace it with 

~~~
 productImageUrl: function productImageUrl(productId) {
            var url = 'img/products/' + productId + '.png';
            return this.imagesMappings.hasOwnProperty(url) ? this.imagesMappings[url] : url;
}
~~~