package com.ampersanda.alauncher;

/**
 * Created by ampersanda on 04/12/17.
 */

public interface ActivityCreated {
    void onBindView();
    void onBindString();
    void onBindListener();
}
