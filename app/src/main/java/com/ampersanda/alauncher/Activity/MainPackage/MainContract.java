package com.ampersanda.alauncher.Activity.MainPackage;

import android.view.View;

/**
 * Created by ampersanda on 04/12/17.
 */

public interface MainContract {
    void onShowAppsClicked(View view);
}
