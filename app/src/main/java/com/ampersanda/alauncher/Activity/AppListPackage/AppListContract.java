package com.ampersanda.alauncher.Activity.AppListPackage;

/**
 * Created by ampersanda on 04/12/17.
 */

public interface AppListContract {
    void loadApps();
}
