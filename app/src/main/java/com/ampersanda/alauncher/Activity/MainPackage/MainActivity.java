package com.ampersanda.alauncher.Activity.MainPackage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.ampersanda.alauncher.ActivityCreated;
import com.ampersanda.alauncher.Activity.AppListPackage.AppListActivity;
import com.ampersanda.alauncher.R;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends Activity implements ActivityCreated {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        onBindView();
        onBindString();
        onBindListener();

        startActivity(new Intent(getApplicationContext(), AppListActivity.class));
        finish();
    }

    @Override
    public void onBindView() {
    }

    @Override
    public void onBindString() {

    }

    @Override
    public void onBindListener() {
    }
}