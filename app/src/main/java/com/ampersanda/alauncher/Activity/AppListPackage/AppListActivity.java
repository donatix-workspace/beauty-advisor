package com.ampersanda.alauncher.Activity.AppListPackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.VideoView;

import com.ampersanda.alauncher.ActivityCreated;
import com.ampersanda.alauncher.Object.CustomViewGroup;
import com.ampersanda.alauncher.R;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Timer;
import java.util.TimerTask;

public class AppListActivity extends Activity implements ActivityCreated, AppListContract {
    private final String hostname = "62.168.217.33";
    private final String url = "https://ebeauty.nivea.bg/";
//    private final String url = "http://ebeauty.donatix.info/";
    private final String offlineUrl = "file:///android_asset/html/index.html";
    public static boolean isReachable = false;
    public boolean isCurrentlyReachable = true;

    private Context context = AppListActivity.this;

    private WebView mWebview;

    private VideoView mVideoView;

    private PowerManager.WakeLock mWakeLock;

    private boolean isHD = true;

    private Handler mHandler;

    private Runnable mRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("", ">>>> onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_list);

        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        isHD = display.getWidth() == 1920;

        onBindView();
        onBindString();
        onBindListener();

        loadApps();

        final View activityRootView = mWebview;
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        });

        loadVideo();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, 0);
            final Handler handle = new Handler();
            Runnable delay = new Runnable() {
                public void run()
                {
                    finish();
                }
            };
            handle.postDelayed(delay,2000);
        }
        else {
            preventStatusBarExpansion(context);
        }
    }

    @Override
    protected void onResume() {
        Log.d("", ">>>> onResume");
        super.onResume();

        PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "myapp:mywakelocktag");
        mWakeLock.acquire();

        mVideoView.start();
    }

    @Override
    protected void onPause() {
        Log.d("", ">>>> onPause");
        super.onPause();

        mWakeLock.release();

        mVideoView.stopPlayback();
    }

    @Override
    public void onBindView() {
        mWebview = findViewById(R.id.webview);
        mVideoView = findViewById(R.id.video_view);
    }

    @Override
    public void onBindString() {

    }

    @Override
    public void onBindListener() {

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
    private int getScale(){
        Double val = isHD ? 1.0 : 0.538;
        val = val * 100d;
        return val.intValue();
    }
    @Override
    public void loadApps() {

//        mWebview = new WebView(this);

        mWebview.clearCache(true);
        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        mWebview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        mWebview.setLongClickable(false);
        mWebview.setHapticFeedbackEnabled(false);
        mWebview.setPadding(0, 0, 0, 0);
        mWebview.setInitialScale(getScale());

        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Log.d("", ">>>> onReceivedError");
                if (request.isForMainFrame()) {
                    view.loadUrl(getCurrentUrl());
                }
                else {
                    super.onReceivedError(view, request, error);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                checkVenueName();
            }
        });

        mWebview.loadUrl(getCurrentUrl());

        mWebview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                scheduleScreenSaver();
                isHostReachable();
                if (isCurrentlyReachable != isReachable) {
                    isCurrentlyReachable = isReachable;
                    mWebview.loadUrl(getCurrentUrl());
                    return true;
                }
                return false;
            }
        });

//        if(!isHD) {
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(1024,640);
//            params.setMargins(0, -20, 0, -20);
//
//            setContentView(mWebview,params );
//        }
//        else {
//            setContentView(mWebview);
//        }
    }

    private static boolean isTrying = false;
    private void isHostReachable() {
        if(isTrying)
            return;
        isTrying = true;

        new AsyncTask(){
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    SocketAddress sockaddr = new InetSocketAddress(hostname, 80);
                    // Create an unbound socket
                    Socket sock = new Socket();

                    // This method will block no more than timeoutMs.
                    // If the timeout occurs, SocketTimeoutException is thrown.
                    int timeoutMs = 5000;   // 5 seconds
                    sock.connect(sockaddr, timeoutMs);
                    AppListActivity.isReachable = true;
                    sock.close();
                } catch (Exception e) {
                    AppListActivity.isReachable = false;
                } finally {
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                AppListActivity.isTrying = false;
            }
        }.execute();
    }

    private String getCurrentUrl() {
        return isReachable ? url : offlineUrl;
    }

    private void checkVenueName() {
        SharedPreferences preferences = context.getSharedPreferences("NiveaAdvisor", Context.MODE_PRIVATE);
        String name = preferences.getString("venueName", null);

        if(name == null) {
            promptForVenueName();
        } else {
            applyVenueName(name);
        }
    }

    private void applyVenueName(String name) {
        SharedPreferences preferences = context.getSharedPreferences("NiveaAdvisor", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("venueName", name);
        editor.commit();

        mWebview.loadUrl("javascript:(function () { " +
                "venueName = '" + name + "';" +
                "})()");
    }

    private void promptForVenueName() {
        final Context context = this.context;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Въведете име");

        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint("Въведете име на търговският обект.");
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Запази", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String text = input.getText().toString();
                applyVenueName(text);
            }
        });
        builder.setNegativeButton("Отказ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void loadVideo() {
        VideoView view = this.mVideoView;
        String path = "android.resource://" + getPackageName() + File.separator + "raw" + File.separator + "video";

        view.setVideoURI(Uri.parse(path));
        view.requestFocus();
        view.start();
        mWebview.setVisibility(View.GONE);

        view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mWebview.loadUrl(getCurrentUrl());
                mVideoView.setVisibility(View.GONE);
                mWebview.setVisibility(View.VISIBLE);
                mVideoView.stopPlayback();
                scheduleScreenSaver();

                return true;
            }
        });
    }

    private void scheduleScreenSaver() {
        if(mHandler != null && mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }

        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                mVideoView.requestFocus();
                mVideoView.start();
                mVideoView.setVisibility(View.VISIBLE);
                mWebview.setVisibility(View.GONE);
            }
        };
        mHandler.postDelayed(mRunnable, 60*1000);
    }

    public void preventStatusBarExpansion(Context context) {
        WindowManager manager = ((WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL|WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;

        int resId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        int result = 0;
        if (resId > 0) {
            result = context.getResources().getDimensionPixelSize(resId);
        } else {
            // Use Fallback size:
            result = 60; // 60px Fallback
        }

        localLayoutParams.height = result;
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        CustomViewGroup view = new CustomViewGroup(context, this);
        manager.addView(view, localLayoutParams);
    }

    public void presentPasswordPrompt() {

//        Intent intent = new Intent(Settings.ACTION_SETTINGS);//ACTION_HOME_SETTINGS);
//        startActivity(intent);

        final Context context = this.context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Въведете парола");

        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint("Въведете парола за достъп до настройки.");
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Вход", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String text = input.getText().toString();
                if(text.equalsIgnoreCase("bgnivea")) {
                    Dialog dialog1 = (Dialog) dialog;
                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    dialog1.getOwnerActivity().startActivity(intent);
                }
            }
        });
        builder.setNegativeButton("Отказ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog alert = builder.create();
        alert.setOwnerActivity(this);
        alert.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                alert.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just stop the timer thread, otherwise, you may receive a crash report
            }
        }, 30000);
    }
}
