package com.ampersanda.alauncher.Activity.AppListPackage;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ampersanda.alauncher.Object.AppDetail;
import com.ampersanda.alauncher.R;

import java.util.List;

/**
 * Created by ampersanda on 04/12/17.
 */

public class AppListAdapter extends RecyclerView.Adapter<AppListAdapter.ViewHolder> {

    private Context context;
    private List<AppDetail> appDetails;
    private OnItemClickListener onItemClickListener;
    private PackageManager manager;

    public interface OnItemClickListener{
        void onItemClick(AppDetail appDetail);
        void onItemLongClick(AppDetail appDetail);
    }

    public AppListAdapter(Context context, List<AppDetail> appDetails, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.appDetails = appDetails;
        this.onItemClickListener = onItemClickListener;

        manager = context.getPackageManager();
    }

    @Override
    public AppListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppListAdapter.ViewHolder holder, int position) {
        AppDetail appDetail = appDetails.get(position);
        holder.bind(appDetail, onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return appDetails != null ? appDetails.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView appImage;
        TextView appLabel;
        View appItem;

        public ViewHolder(View itemView) {
            super(itemView);

            appItem = itemView.findViewById(R.id.recycler_item);
            appImage = itemView.findViewById(R.id.recycler_item_app_icon);
            appLabel = itemView.findViewById(R.id.recycler_item_app_label);
        }

        public void bind(final AppDetail appDetail, final OnItemClickListener onItemClickListener) {
            appImage.setImageDrawable(appDetail.getIcon());
            appLabel.setText(appDetail.getLabel());

            appItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(appDetail);
                }
            });

            appItem.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    onItemClickListener.onItemLongClick(appDetail);

                    return true;
                }
            });
        }
    }
}
