package com.ampersanda.alauncher.Utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by ampersanda on 04/12/17.
 */

public class RecyclerUtils {
    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / 120);
    }
}
