package com.ampersanda.alauncher.Object;

import android.content.Context;
import android.view.MotionEvent;
import android.view.ViewGroup;

import com.ampersanda.alauncher.Activity.AppListPackage.AppListActivity;

public class CustomViewGroup extends ViewGroup {
    private AppListActivity activity;

    public CustomViewGroup(Context context, AppListActivity activity) {
        super(context);
        this.activity = activity;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        // Intercepted touch!
        this.activity.presentPasswordPrompt();
        return true;
    }
}
