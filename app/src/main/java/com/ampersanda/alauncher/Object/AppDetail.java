package com.ampersanda.alauncher.Object;

import android.graphics.drawable.Drawable;

/**
 * Created by ampersanda on 04/12/17.
 */

public class AppDetail {
    public CharSequence packageName;
    public Drawable icon;
    public CharSequence label;

    public AppDetail(CharSequence packageName, Drawable icon, CharSequence label) {
        this.packageName = packageName;
        this.icon = icon;
        this.label = label;
    }

    public CharSequence getPackageName() {
        return packageName;
    }

    public Drawable getIcon() {
        return icon;
    }

    public CharSequence getLabel() {
        return label;
    }
}
